﻿using System;
using UnityEngine;

public class Key : MonoBehaviour
{
    [SerializeField] private string _key;
    private bool _isActive;
    private KeyCode _code;

    private void Awake()
    {
        Enum.TryParse(_key, out _code);
    }

    private void Update()
    {
        _isActive = Input.GetKey(_code);
    }

    public bool GetKeyState()
    {
        return _isActive;
    }

    public string GetKeySymbol()
    {
        return _key;
    }
}
