using System.Collections;
using System.Collections.Generic;
using Settings;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameProcessing : MonoBehaviour
{
    private const int MaxActiveKeyValue = 5;
    
    [SerializeField] private ConfigSettings _configSettings;
    [SerializeField] private CommonSettings _commonSettings;
    [SerializeField] private Spawner _spawner;
    [SerializeField] private Slider _progressBar;
    [SerializeField] private GameObject _leftMouseButtonSprite;
    [SerializeField] private GameObject _rightMouseButtonSprite;
    [SerializeField] private List<Text> _keysSymbols;
    [SerializeField] private List<Image> _keysImage;
    [SerializeField] private GameObject _budda;
    
    private List<Key> _allKeys;
    private List<Key> _activeKeys = new List<Key>();
    private List<bool> _activeKeysStates;
    private List<Thought> _thoughts = new List<Thought>();

    private float _timer;
    private float _startGameTimer;
    private float _progressBarValue;
    private int _nextKeyIndex = 1;
    private bool _isFirstThoughtSpawned;
    private bool _isFirstSymbolSet;
    private bool _isWin;
    private bool _isBuddaFly;

    private void Start()
    {
        InitKeysList();
        InitKeysStatesList();
        _commonSettings.SetThoughtSpeed(1);
        _progressBar.maxValue = _allKeys.Count * _commonSettings.NextKeyTime;
    }

    private void Update()
    {
        if (UIManager.Instance.IsStartGame())
        {
            var leftMouseButton = Input.GetMouseButton(0);
            var rightMouseButton = Input.GetMouseButton(1);
            _leftMouseButtonSprite.SetActive(leftMouseButton);
            _rightMouseButtonSprite.SetActive(rightMouseButton);

            _startGameTimer += Time.deltaTime;
            if (_startGameTimer > 3f)
                GameProcess();
        }
    }

    private void GameProcess()
    {
        InitFirstThought();
        SetFirstKeySymbol();
        if (CheckKeys() && CheckMouseButtons())
        {
            _timer += Time.deltaTime;
            _progressBarValue += Time.deltaTime;
            SetThoughtsMovable();
            if (_timer > _commonSettings.NextKeyTime)
            {
                if (_nextKeyIndex < _allKeys.Count)
                {
                    ActivateNextKey();
                    SetKeySymbols();
                    InitNewThought();
                    if (_thoughts.Count > MaxActiveKeyValue)
                        DestroyUnusedThought();
                    _nextKeyIndex++;
                    _timer = 0;                
                }
            }
        }
        else
        {
            SetThoughtsMovable();
            if (Target.Instance.IsTouched() && !_isWin)
            {
                UIManager.Instance.EndGameState();
            }
        }
        
        _progressBar.value = _progressBarValue;

        if (_progressBarValue > _progressBar.maxValue)
        {
            Win();
        }
    }

    private void InitKeysList()
    {
        _allKeys = new List<Key>();
        var index = Random.Range(0, _configSettings.ConfigSettingsList.Count);

        for (int i = 0; i < _configSettings.ConfigSettingsList[index].Keys.Count; i++)
        {
            var currentKey = Instantiate(_configSettings.ConfigSettingsList[index].Keys[i], transform.position, Quaternion.identity);
            if (i != 0)
                currentKey.gameObject.SetActive(false);
            else
                _activeKeys.Add(currentKey);
            _allKeys.Add(currentKey);
        }
    }
    
    private void InitKeysStatesList()
    {
        _activeKeysStates = new List<bool>();
        for (int i = 0; i < MaxActiveKeyValue; i++)
        {
            _activeKeysStates.Add(true);
        }
    }

    private void ActivateNextKey()
    {
        _activeKeys.Add(_allKeys[_nextKeyIndex]);
        _allKeys[_nextKeyIndex].gameObject.SetActive(true);
        
        if (_nextKeyIndex < MaxActiveKeyValue) return;

        _activeKeys.RemoveAt(0);
        _allKeys[_nextKeyIndex - MaxActiveKeyValue].gameObject.SetActive(false);
        _spawner.VacateSpawnPoint();
    }

    private bool CheckKeys()
    {
        for (int i = 0; i < _activeKeys.Count; i++)
        {
            _activeKeysStates[i] = _activeKeys[i].GetKeyState();
            SetKeyColor(_activeKeysStates[i], i);
        }

        return !_activeKeysStates.Contains(false);
    }

    private void SetKeyColor(bool state, int index)
    {
        _keysImage[index].color = state ? Color.red : Color.white;
    }
    
    private bool CheckMouseButtons()
    {
        var leftMouseButton = Input.GetMouseButton(0);
        var rightMouseButton = Input.GetMouseButton(1);
        return leftMouseButton && rightMouseButton;
    }

    private void InitNewThought()
    {
        var currentThought = _spawner.InstantiateThought();
        currentThought.SetMovable(true);
        _thoughts.Add(currentThought);
    }

    private void InitFirstThought()
    {
        if (!_isFirstThoughtSpawned)
        {
            InitNewThought();
            _isFirstThoughtSpawned = true;
        }
    }

    private void SetThoughtsMovable()
    {
        for (int i = 0; i < _activeKeys.Count; i++)
        {
            if (CheckMouseButtons())
                _thoughts[i].SetMovable(!_activeKeysStates[i]);
            else
                _thoughts[i].SetMovable(true);
        }
    }

    private void SetThoughtsState(bool state)
    {
        foreach (var thought in _thoughts)
        {
            thought.SetMovable(state);
        }
    }

    private void DestroyUnusedThought()
    {
        Destroy(_thoughts[0].gameObject);
        _thoughts.RemoveAt(0);
    }

    private void SetKeySymbols()
    {
        for (int i = 0; i < _activeKeys.Count; i++)
        {
            _keysSymbols[i].text = _activeKeys[i].GetKeySymbol();
        }
    }

    private void SetFirstKeySymbol()
    {
        if (!_isFirstSymbolSet)
        {
            SetKeySymbols();
            _isFirstSymbolSet = true;
        }
    }

    private void Win()
    {
        if (!_isBuddaFly)
            _budda.transform.Translate(Vector3.up * 1 * Time.deltaTime);
        if (!_isWin)
        {
            UIManager.Instance.WinGameState();
            DestroyAllThoughts();
            StartCoroutine(Wait(2f));
            _isWin = true;
        }
    }
    
    private IEnumerator Wait(float sec)
    {
        yield return new WaitForSeconds(sec);
        ThoughtArmageddon();
        _isBuddaFly = true;
    }

    private void ThoughtArmageddon()
    {
        for (int i = 0; i < 5; i++)
        {
            _spawner.VacateSpawnPoint();
            InitNewThought();
            _commonSettings.SetThoughtSpeed(10);
        }
        UIManager.Instance.WinEndGameState();
    }

    private void DestroyAllThoughts()
    {
        foreach (var thought in _thoughts)
        {
            Destroy(thought.gameObject);
            _spawner.VacateSpawnPoint();
        }
    }
}
