﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public static Target Instance;
    private bool _isTouched;

    private void Awake()
    {
        Instance = this;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Thought"))
            _isTouched = true;
    }

    public bool IsTouched()
    {
        return _isTouched;
    }
}
