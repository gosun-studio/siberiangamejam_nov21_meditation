using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ThoughtTextPool", menuName = "Meditation/ThoughtTextPool", order = 0)]
public class ThoughtTextPool : ScriptableObject
{
    [SerializeField] private List<string> _thoughtTextList;

    public List<string> ThoughtTextList => _thoughtTextList;
}
