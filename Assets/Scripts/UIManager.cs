using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    
    [SerializeField] private GameObject _menu;
    [SerializeField] private GameObject _game;
    [SerializeField] private GameObject _winGame;
    [SerializeField] private GameObject _winEndGame;
    [SerializeField] private GameObject _endGame;

    private int _fastStartMode;
    private bool _isStartGame;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _menu.SetActive(true);
        _game.SetActive(false);
        _winGame.SetActive(false);
        _winEndGame.SetActive(false);
        _endGame.SetActive(false);
        
        _fastStartMode = PlayerPrefs.GetInt("fastMode", 0);

        if (_fastStartMode == 1)
        {
            StartGame();
            _isStartGame = true;
        }
    }

    private void StartGame()
    {
        _menu.SetActive(false);
        _game.SetActive(true);
        _winGame.SetActive(false);
        _winEndGame.SetActive(false);
        _endGame.SetActive(false);
    }
    
    public void WinGameState()
    {
        _menu.SetActive(false);
        _game.SetActive(false);
        _winGame.SetActive(true);
        _winEndGame.SetActive(false);
        _endGame.SetActive(false);
    }

    public void WinEndGameState()
    {
        _menu.SetActive(false);
        _game.SetActive(false);
        _winGame.SetActive(false);
        _winEndGame.SetActive(true);
        _endGame.SetActive(false);
    }

    public void EndGameState()
    {
        _menu.SetActive(false);
        _game.SetActive(false);
        _winGame.SetActive(false);
        _winEndGame.SetActive(false);
        _endGame.SetActive(true);
    }
    
    public void OnPlayClick()
    {
        StartGame();
        _isStartGame = true;
    }
    
    public void OnReset()
    {
        SceneManager.LoadScene("Main");
        PlayerPrefs.SetInt("fastMode", 1);
    }
    
    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("fastMode", 0);
    }

    public void OnQuit()
    {
        Application.Quit();
    }

    public bool IsStartGame()
    {
        return _isStartGame;
    }
}
