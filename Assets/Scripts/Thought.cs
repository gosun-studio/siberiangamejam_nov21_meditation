using Settings;
using UnityEngine;
using Random = UnityEngine.Random;

public class Thought : MonoBehaviour
{
    [SerializeField] private TextMesh _text;
    [SerializeField] private GameObject _target;
    [SerializeField] private ThoughtTextPool _textPool;
    [SerializeField] private CommonSettings _commonSettings;

    private bool _isMove;

    private void Start()
    {
        _text.text = _textPool.ThoughtTextList[Random.Range(0, _textPool.ThoughtTextList.Count)];
    }
    
    private void Update()
    {
        Move();
    }

    public void SetMovable(bool state)
    {
        _isMove = state;
    }

    private void Move()
    {
        if (_isMove)
            transform.position = Vector2.MoveTowards(transform.position, _target.transform.position, _commonSettings.ThoughtSpeed * Time.deltaTime);
    }
}
