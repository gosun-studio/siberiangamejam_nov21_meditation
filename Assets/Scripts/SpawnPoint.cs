﻿using System;
using UnityEngine;

[Serializable]
public class SpawnPoint
{
    public GameObject Point;
    public bool IsOccupied;
}