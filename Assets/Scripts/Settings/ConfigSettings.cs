using System;
using System.Collections.Generic;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "ConfigSettings", menuName = "Meditation/ConfigSettings", order = 0)]
    public class ConfigSettings : ScriptableObject
    {
        [SerializeField] private List<ConfigSettingsList> _configSettingsList;

        public List<ConfigSettingsList> ConfigSettingsList => _configSettingsList;
    }

    [Serializable]
    public class ConfigSettingsList
    {
        public List<Key> Keys;
    }
}
