﻿using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "CommonSettings", menuName = "Meditation/CommonSettings", order = 0)]
    public class CommonSettings : ScriptableObject
    {
        [SerializeField] private float _thoughtSpeed;
        [SerializeField] private float _nextKeyTime;

        public float ThoughtSpeed => _thoughtSpeed;
        public float NextKeyTime => _nextKeyTime;

        public void SetThoughtSpeed(float speed)
        {
            _thoughtSpeed = speed;
        }
    }
}