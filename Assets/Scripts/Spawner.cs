using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<SpawnPoint> _spawnPoints;
    [SerializeField] private GameObject _thought;

    private int _vacateSpawnPointIndex;

    public Thought InstantiateThought()
    {
        var currentThought = Instantiate(_thought, GetFreeSpawnPoint().transform.position, Quaternion.identity);
        return currentThought.GetComponent<Thought>();
    }

    private GameObject GetFreeSpawnPoint()
    {
        var freePoint = _spawnPoints.First(sp => !sp.IsOccupied);
        freePoint.IsOccupied = true;
        return freePoint.Point;
    }

    public void VacateSpawnPoint()
    {
        _spawnPoints[_vacateSpawnPointIndex].IsOccupied = false;
        _vacateSpawnPointIndex++;
        if (_vacateSpawnPointIndex > 4)
            _vacateSpawnPointIndex = 0;
    }
}
